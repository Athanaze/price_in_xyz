import json

bad_words = ["hedge", "long", "short", "wrapped", "=", "+", "(", ")"]
li = []
with open('list.json') as f:
    data = json.load(f)
    for d in data:
        name = d["name"].lower()
        bad = False
        for w in bad_words:
            if w in name:
                bad = True
        if not bad:
            s = d["symbol"].upper()
            if s not in li:
                print(
                    '{ value: "'+ s+'", label: "'+s+'", group: "Crypto" },'
                )
                li.append(s)