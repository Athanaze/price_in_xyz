import json

with open("markets.json") as f:
    data = json.load(f)
    for d in data:
        s = d["symbol"].upper()
        
        print('{ value: "'+s+'", label: "'+s+'", group: "Crypto" },')