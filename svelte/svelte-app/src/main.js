import "carbon-components-svelte/css/all.css";

import App from './App.svelte';

var app = new App({
	target: document.body
});

export default app;